const infoboire = 'boire';
const infomanger ='manger';
const infojouer ='jouer';
const infohelp = 'help';
const infovitesse ='vitesse';
const infotir = 'tir';
const ongletConsole = document.getElementById('onglet');
const info1 = '1';
const info2 = '2';
const info3 = '3';
const info4 = '4';
const info5 = '5';
const infomort = 'suicide';
const root = document.documentElement;
const ver = document.getElementById('mon-worms');
const verCoule = document.getElementById('mon-worms-coule');
const verCoule2 = document.getElementById('mon-worms-coule2');
const inputName = document.getElementById('name');
const enterName = document.getElementById('enter-name');
const btnOk = document.getElementById('btn-ok');
const nomFinal = document.getElementById('nom-final');
const sante = document.getElementById('vie');
const simplon = document.getElementById('simplon');
const zonetexte = document.getElementById('zonetext');
const console = document.getElementById('console');
const soif = document.getElementById('barre-soif');
const para = document.getElementById('worm-para');
const crate = document.getElementById('crate');
const paracrate = document.getElementById('para-crate');
const eauico = document.getElementById('eau-ico');
const pigeonico = document.getElementById('pigeon-ico');
const nuage1 = document.getElementById('nuage1');
const nuage2 = document.getElementById('nuage2');
const pluie = document.getElementById('pluie');
const baseico = document.getElementById('batte-ico');
const monpigeon=document.getElementById('pigeon');
const cross = document.getElementById('cross');
const reloadarrow = document.getElementById('reload')
const etat=document.getElementById('etat');
const ennuie = document.getElementById('barre-ennuie');
const mort = document.getElementById('mort');
const timerpluie = 7;
const tempmove3 = 100;
const speed = 3;
const textconsole = console.value;

let nomduver;
let tempmove1 = 600;
let tempmove2 = 300;
let niveausante = 200;
let niveauennuie =200;
let niveausoif = 200;
let parachute = 3;
let pluieonoff = false;
let consoleOnOff = false;
let tamponConsole;
let varsoif = -1;
let varennuie = -1;
let varsante = -1;
const tabCon = [];
let h=0;
const paracratebool = false;
let baseballmode=false;



eauico.addEventListener('click', pluiestart);
eauico.addEventListener('click', parler);
crate.addEventListener('click', soin);
baseico.addEventListener('click', baseball);
btnOk.addEventListener('click', validnom);
onglet.addEventListener('click', ouvertureOnglet);
pigeonico.addEventListener('click', pigeonshoot);
cross.addEventListener('click', (event) => {help.style.visibility='hidden';});
reloadarrow.addEventListener('click', reload);


function validnom() {
  nomduver=inputName.value;

  enterName.style.animationPlayState='running';
  inputName.style.left="-15px"
  inputName.style.top="0px"
  inputName.style.paddingLeft="10px"
  inputName.style.fontSize="14px "
  inputName.innerText="nomduver"


  sante.style.display='block';
  btnOk.style.visibility='hidden';
  inputName.style.display='block  ';
  simplon.style.visibility='visible';
}


window.addEventListener('keydown', function(event) {
  switch (event.key) {
    case 'ArrowLeft':
      document.getElementById('mon-worms').style.animationPlayState='running';
      ver.style.backgroundImage= 'url(images/marchegauche.gif)';
      setInterval(deplacementgauche(), 10000);

      function deplacementgauche() {
        tempmove1 = tempmove1 - speed;
        tempmove2 = tempmove2 - speed;
        document.documentElement.style.setProperty('--worm-pos1', tempmove1-10+'px');
        document.documentElement.style.setProperty('--worm-pos4', tempmove2-10+'px');


        root.style.setProperty('--worm-pos3', tempmove1-10 +'px');

        if (tempmove1 < 150) {
          ver.style.display='none';
          verCoule.style.display='block';
          verCoule.style.animationPlayState='running';
          const timer = setInterval(function() {
            niveausante = niveausante - 10; sante.style.width= niveausante +'px';
          }, 100);
        }
      }
      break;


    case 'ArrowRight':
      ver.classList.remove('versoin');
      document.getElementById('mon-worms').style.animationPlayState='running';
      ver.style.backgroundImage= 'url(images/marchedroite.gif)';
      setInterval(deplacementdroite(), 10000);

      function deplacementdroite() {
        tempmove1 = tempmove1 + speed;
        tempmove2 = tempmove2 + speed;

        root.style.setProperty('--worm-pos4', (tempmove1-132) +'px');
        root.style.setProperty('--worm-pos3', (tempmove1-67) +'px');
        document.documentElement.style.setProperty('--worm-pos1', tempmove1+'px');
        document.documentElement.style.setProperty('--worm-pos4', tempmove2+'px');


        if (tempmove1 > 1020) {
          ver.style.display='none';
          verCoule2.style.display='block';
          verCoule2.style.animationPlayState='running';
          const timer = setInterval(function() {
            niveausante = niveausante - 10; sante.style.width= niveausante+'px';
          }, 100);
        }
      }
      break;
  }
});


window.addEventListener('keyup', function(event) {
  switch (event.key) {
    case 'ArrowLeft':
      ver.style.backgroundImage= 'url(images/gauchecentre.gif)';
      document.getElementById('mon-worms').style.animationPlayState='paused';
      break;
  }
});

window.addEventListener('keyup', function(event) {
  switch (event.key) {
    case 'ArrowRight':
      ver.style.backgroundImage= 'url(images/droitecentre.gif)';
      document.getElementById('mon-worms').style.animationPlayState='paused';
      break;
  }
});


window.addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    h++;
    const listeconsole = document.createElement('li');
    listeconsole.setAttribute('class', 'text');
    listeconsole.setAttribute('id', 'case'+h);
    listeconsole.innerText = console.value;
    listeconsole.addEventListener('click', function() {
      console.value = listeconsole.innerText;
    });
    tabCon[h]= listeconsole.innerText;
    zonetext.prepend(listeconsole);
    tamponConsole = console.value;
    if(tamponConsole != infoboire && tamponConsole != infomanger && tamponConsole != infojouer && tamponConsole != infovitesse && tamponConsole !=info1 && tamponConsole !=info2 && tamponConsole !=info3 && tamponConsole !=info4 && tamponConsole !=info5 && tamponConsole !=infohelp && tamponConsole !=infomort && tamponConsole !=infotir){parler('Je ne vois pas de quoi tu parles ?!')}
    if ( tamponConsole == infoboire ) {
      pluiestart();
    }
    else if ( tamponConsole == infomanger ) {
      soin(); console.value ='je mange';
    }
    else if ( tamponConsole == infojouer ) {
      baseball(); console.value ='je joue';
    }
    else if ( tamponConsole == infohelp) {
      help.style.visibility='visible';
    }
    else if ( tamponConsole == infovitesse) {
      parler('Tapez :  (Tres lent) 1  à  5 (Rapide) ');
    }
    else if ( tamponConsole === info1) {
      varennuie=-1; varsante=-1; varsoif=-1; parler('Vitesse du jeux à 1 (Lent)');
    }
    else if ( tamponConsole === info2) {
      varennuie=-2; varsante=-2; varsoif=-2; parler('Vitesse du jeux à 2 (Lent)');
    }
    else if ( tamponConsole === info3) {
      varennuie=-3; varsante=-3; varsoif=-3; parler('Vitesse du jeux à 3 (moyenne)');
    }
    else if ( tamponConsole === info4) {
      varennuie=-4; varsante=-4; varsoif=-4; parler('Vitesse du jeux à 4 (Rapide)');
    }
    else if ( tamponConsole === info5) {
      varennuie=-5; varsante=-5; varsoif=-5; parler('Vitesse du jeux à 5 (Rapide)');
    }
    else if ( tamponConsole == infomort) {
      niveauennuie = 1; niveausante = 1; niveausoif = 1;
    }
    else if ( tamponConsole == infotir && baseballmode == true) pigeonshoot();
    }
});

window.addEventListener('keydown', (event) => {
  if (event.keyCode === 38) {
    h--;
    if (h <=0) {
      h=1;
    }
    console.value = tabCon[h];

  }
});


window.addEventListener('keydown', (event) => {
  if (event.keyCode === 40) {
    h++;
    if (h>=tabCon.length) {
      h=tabCon.length-1;
    }
    console.value = tabCon[h];
    // document.getElementById("case"+h).style.color="red"
    //  document.getElementById("case"+h).style.color="white"
  }
});


window.addEventListener('keyup', (event) => {
  if (event.keyCode === 13) {
    console.value = '';
  }
});


function ouvertureOnglet() {
  if (consoleOnOff == false) {
    consoleOnOff = true;
    onglet.style.animationName = 'onglet';
    onglet.style.animationDuration = '2s';
    onglet.style.animationFillMode =' forwards';

    console.style.animationName = 'console';
    console.style.animationDuration = '2s';
    console.style.animationFillMode =' forwards';

    zonetexte.style.animationName = 'textconsole';
    zonetexte.style.animationDuration = '2s';
    zonetexte.style.animationFillMode =' forwards';
  } else {
    consoleOnOff = false;
    onglet.style.animationName = 'closeOnglet';
    onglet.style.animationDuration = '2s';
    onglet.style.animationFillMode =' forwards';

    console.style.animationName = 'closeConsole';
    console.style.animationDuration = '2s';
    console.style.animationFillMode =' forwards';

    zonetexte.style.animationName = 'texteconsoleclose';
    zonetexte.style.animationDuration = '2s';
    zonetexte.style.animationFillMode =' forwards';
  }
}


function pluiestart() {
  if ( pluieonoff === false) {
    eauico.src ='images/eau.png';
    varsoif = +3;
    pluieonoff = true;
    nuage1.style.animation = 'pluie 5s forwards';
    nuage2.style.animation='pluie 7s forwards';
    pluie.style.animation='goutte 7s forwards';
    if (niveausoif >= 200) {
      varsoif = -2;
    }
    parler('De l\'eauu !!! OowiiiiiiiII');
  } else {
    eauico.src ='images/starteau.png';
    varsoif = -2;
    pluieonoff = false;
    nuage1.style.animation='pluiefin 7s forwards';
    nuage2.style.animation='pluiefin 7s forwards';
    pluie.style.animation='gouttefin 7s forwards';
    parler('Cette pluie était très agréable !!');
  }
}


var parler = function parler(dialogue) {
  etat.innerText= dialogue;
  etat.classList.remove('parler');
  void paracrate.offsetWidth; // trigger a DOM reflow
  etat.classList.add('parler');
};


function soin() {
  parler('Des soins Vite !!!!');
  paracrate.style.visibility='visible';
  enterName.style.visibility='hidden';
  paracrate.src='images/monparachute.png';
  paracrate.classList.remove('animate');
  void paracrate.offsetWidth; // trigger a DOM reflow
  paracrate.classList.add('animate');
  mafonction;
  function mafonction() {
    paracrate.src='images/monparachutefin.png';
    ver.classList.remove('versoin');
    void ver.offsetWidth; // trigger a DOM reflow
    ver.classList.add('versoin');

    mafonction2;
    function mafonction2() {
      enterName.style.visibility='visible';
      paracrate.style.visibility='hidden';
      if (niveausante < 150) {
        niveausante=niveausante+50;
      } else {
        niveausante = 200;
      }
    }
    setTimeout(mafonction2, 3000);
  }
  setTimeout(mafonction, 5000);
}


function baseball() {
  if (baseballmode == false) {
    parler('Il ne manque plus qu\'une balle !');
    pigeonico.style.animation='slidepigeon 1s forwards';
    baseico.src='images/batteuse.png';
    void ver.offsetWidth; // trigger a DOM reflow
    ver.style.backgroundImage= 'url(images/basedroite.gif)';
    baseballmode=true;
  } else {
    parler('A qui le tour ! ? ');
    pigeonico.style.animation='slidepigeonend 1s forwards';
    baseico.src='images/batte.png';
    baseballmode=false;
    ver.style.backgroundImage= 'url(images/droitecentre.gif)';
  }
}
function pigeonshoot() {
  parler('HooOoOoMmMmeEe RRuuuUUNNNN !!!!!');
  ver.style.backgroundImage='url(images/basedroite.gif)';
  pigeon.style.visibility='visible';
  pigeon.classList.remove('pigeonanim');
  void paracrate.offsetWidth; // trigger a DOM reflow
  pigeon.classList.add('pigeonanim');
  coupdebatte;
  function coupdebatte() {
    ver.style.backgroundImage='url(images/basetir1.gif)';
    ver.style.width='120px';
    niveauennuie=niveauennuie+30;
  }
  setTimeout(coupdebatte, 2000);
}


const timer = setInterval(function() {
  parachute = parachute - 1; if (parachute <1 ) {
    ver.style.visibility='visible';
  }
}, 1000);

const timersoif = setInterval(function() {
  niveausoif = niveausoif + varsoif; soif.style.width= niveausoif +'px';
  if (niveausoif >= 200) {
    niveausoif =200; varsoif = -2;
  }
  if (niveausoif <= 120 && niveausoif > 117 ) {
    soif.style.backgroundColor = 'yellow'; parler('Cela fait un moment que je n\'ai rien bu ...');
  } else {
    soif.style.backgroundColor ='#4285F4';
  }
  if (niveausoif <= 60 && niveausoif > 57) {
    soif.style.backgroundColor = 'red!important'; parler('De la bière , du genepi, du rhum ... n\'importe quoi SvP !! Je meurs de soif !');
  };
  if (niveausante <= 1 && niveauennuie <= 1 && niveausoif <= 1) {
    mort.style.visibility = 'visible';reloadarrow.style.visibility="visible"; mort.innerText= nomduver+' est mort !! Quel dommage !';
  };
}, 1000 );

const timerennuie = setInterval(function() {
  niveauennuie = niveauennuie + varennuie; ennuie.style.width= niveauennuie +'px';
  if (niveauennuie >= 200) {
    niveauennuie =200; varennuie = -2;
  }
  if (niveauennuie <= 120 && niveauennuie > 117 ) {
    ennuie.style.backgroundColor = 'yellow!important'; parler('Tu aimes le baseball ??');
  } else {
    ennuie.style.backgroundColor ='#007E33';
  };
  if (niveauennuie <= 60 && niveauennuie > 57 ) {
    ennuie.style.backgroundColor = 'red!important'; parler('Quelle vie pourrie !!!');
  };
  if (niveausante <= 1 && niveauennuie <= 1 && niveausoif <= 1) {
    mort.style.display = 'block'; mort.innerText= nomduver+' est mort !! Quel dommage !';
  };
}, 1000 );

const timersante = setInterval(function() {
  niveausante = niveausante + varsante; sante.style.width= niveausante +'px';
  if (niveausante >= 200) {
    niveausante = 200; varsante = -2;
  }
  if (niveausante <= 120 && niveausante > 115) {
    sante.style.backgroundColor = 'yellow!important'; parler('Je meur de faim ...');
  };
  if (niveausante <= 60 && niveausante > 57) {
    sante.style.backgroundColor = 'red!important'; parler('A l\'aide ! Je suis vraiment faible !!');
  };
  if (niveausante <= 1 && niveauennuie <= 1 && niveausoif <= 1) {
    mort.style.display = 'block'; mort.innerText= nomduver+' est mort !! Quel dommage !';
  };
}, 1000 );


function reload(){
  niveausante = 200;
  niveauennuie = 200;
  niveausoif = 200;
  mort.style.visibility="hidden"
  reloadarrow.style.visibility="hidden"

  enterName.style.animationName="reductionnominverse"
  inputName.style.left="132px"
  inputName.style.top="135px"
  inputName.style.paddingLeft="10px"
  inputName.style.fontSize="30px "
  inputName.innerText="nomduver"


  sante.style.display='block';
  btnOk.style.visibility='visible';
  inputName.style.display='block  ';
  simplon.style.visibility='visible';
}