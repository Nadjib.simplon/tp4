
var root = document.documentElement;
var ver = document.getElementById('mon-worms');
var verCoule = document.getElementById('mon-worms-coule');
var verCoule2 = document.getElementById('mon-worms-coule2');
var inputName = document.getElementById('name');
var enterName = document.getElementById('enter-name');
var btnOk = document.getElementById('btn-ok');
var nomFinal = document.getElementById('nom-final');
var sante = document.getElementById('vie');
var simplon = document.getElementById('simplon');
var nomduver;
var tempmove1 = 600;
var tempmove2 = 600;
var tempmove3 = 100;
var speed = 3;
var niveausante = 200;
var mort = document.getElementById('mort');
var niveauennuie =200;
var ennuie = document.getElementById('barre-ennuie');
var niveausoif = 200;
var soif = document.getElementById('barre-soif');
var para = document.getElementById('worm-para');
var crate = document.getElementById('crate');
var paracrate = document.getElementById('para-crate');
var eauico = document.getElementById('eau-ico');
var pigeonico = document.getElementById('pigeon-ico');
var nuage1 = document.getElementById('nuage1');
var nuage2 = document.getElementById('nuage2');
var pluie = document.getElementById('pluie');
const timerpluie = 7;
let parachute = 3;
var pluieonoff = false;
var zonetexte = document.getElementById('zonetext');
var console = document.getElementById('console');
var textconsole = console.value;
var consoleOnOff = false;
const infoboire = 'boire';
const infomanger ='manger';
const infojouer ='jouer';
const ongletConsole = document.getElementById('onglet');
var tamponConsole;
let varsoif = -1;
let varennuie = -1;
let varsante = -1;
const tabCon = [];
let h=0;
const etat=document.getElementById('etat');
const paracratebool = false;
const baseballmode=false;
const baseico = document.getElementById('batte-ico');


eauico.addEventListener('click', pluiestart);
crate.addEventListener('click', soin);


const timer = setInterval(function() {
  parachute = parachute - 1; if (parachute <1 ) {
    ver.style.visibility='visible';
  }
}, 1000);


const timersoif = setInterval(function() {
  niveausoif = niveausoif + varsoif; soif.style.width= niveausoif +'px';
  if (niveausoif >= 200) {
    niveausoif =200; varsoif = -2;
  }

  if (niveausoif <= 120 ) {
    soif.style.backgroundColor = 'yellow';
  } else {
    soif.style.backgroundColor ='#4285F4';
  }
  if (niveausoif <= 60 ) {
    soif.style.backgroundColor = 'red!important';
  };
  if (niveausoif <= 1 ) {
    mort.style.display = 'block'; mort.innerText= nomduver+' est mort !! Quel dommage !';
  };
}, 1000 );

const timerennuie = setInterval(function() {
  niveauennuie = niveauennuie + varennuie; ennuie.style.width= niveauennuie +'px';
  if (niveauennuie >= 200) {
    niveauennuie =200; varennuie = -2;
  }
  if (niveauennuie <= 120 ) {
    ennuie.style.backgroundColor = 'yellow!important';
  } else {
    ennuie.style.backgroundColor ='#007E33';
  };
  if (niveauennuie <= 60 ) {
    ennuie.style.backgroundColor = 'red!important';
  };
  if (niveauennuie <= 1 ) {
    mort.style.display = 'block'; mort.innerText= nomduver+' est mort !! Quel dommage !';
  };
}, 1000 );

const timersante = setInterval(function() {
  niveausante = niveausante + varsante; sante.style.width= niveausante +'px';
  if (niveausante >= 200) {
    niveausante = 200; varsante = -2;
  }
  if (niveausante <= 60 ) {
    sante.style.backgroundColor = 'yellow!important';
  };
  if (niveausante <= 30 ) {
    sante.style.backgroundColor = 'red!important';
  };
  if (niveausante <= 1 ) {
    mort.style.display = 'block'; mort.innerText= nomduver+' est mort !! Quel dommage !';
  };
}, 1000 );


function validnom() {
  enterName.style.animationPlayState='running';
  nomduver=document.getElementById('name').value;
  btnOk.style.display='none';
  inputName.style.display='none';
  simplon.style.display='none';
  nomFinal.innerText=nomduver;
  sante.style.display='block';
}

btnOk.addEventListener('click', validnom);
window.addEventListener('keydown', function(event) {
  switch (event.key) {
    case 'ArrowLeft':

      document.getElementById('mon-worms').style.animationPlayState='running';
      ver.style.backgroundImage= 'url(images/marchegauche.gif)';

      setInterval(deplacementgauche(), 10000);


      function deplacementgauche() {
        tempmove1 = tempmove1 - speed;


        document.documentElement.style.setProperty('--worm-pos1', tempmove1-10+'px');
        root.style.setProperty('--worm-pos3', tempmove1-10 +'px');

        if (tempmove1 < 150) {
          ver.style.display='none';
          verCoule.style.display='block';
          verCoule.style.animationPlayState='running';
          const timer = setInterval(function() {
            niveausante = niveausante - 10; sante.style.width= niveausante +'px';
          }, 100);
        }
      }
      break;


    case 'ArrowRight':
      ver.classList.remove('versoin');
      document.getElementById('mon-worms').style.animationPlayState='running';
      ver.style.backgroundImage= 'url(images/marchedroite.gif)';
      setInterval(deplacementdroite(), 10000);
      function deplacementdroite() {
        tempmove1 = tempmove1 + speed;
        root.style.setProperty('--worm-pos4', (tempmove1-132) +'px');
        root.style.setProperty('--worm-pos3', (tempmove1-67) +'px');

        document.documentElement.style.setProperty('--worm-pos1', tempmove1+'px');
        if (tempmove1 > 1020) {
          ver.style.display='none';
          verCoule2.style.display='block';
          verCoule2.style.animationPlayState='running';
          const timer = setInterval(function() {
            niveausante = niveausante - 10; sante.style.width= niveausante+'px';
          }, 100);
        }
      }

      break;
  }
});
window.addEventListener('keyup', function(event) {
  switch (event.key) {
    case 'ArrowLeft':
      ver.style.backgroundImage= 'url(images/gauchecentre.gif)';


      document.getElementById('mon-worms').style.animationPlayState='paused';
      break;
  }
});
window.addEventListener('keyup', function(event) {
  switch (event.key) {
    case 'ArrowRight':
      ver.style.backgroundImage= 'url(images/droitecentre.gif)';


      document.getElementById('mon-worms').style.animationPlayState='paused';
      break;
  }
});


window.addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    h++;
    const listeconsole = document.createElement('li');
    listeconsole.setAttribute('class', 'text');
    listeconsole.setAttribute('id', 'case'+h);
    listeconsole.innerText = console.value;
    listeconsole.addEventListener('click', function() {
      console.value = listeconsole.innerText;
    });
    tabCon[h]= listeconsole.innerText;
    zonetext.prepend(listeconsole);
    tamponConsole = console.value;
    if ( tamponConsole == infoboire ) {
      pluiestart();
    }

    if ( tamponConsole == infomanger ) {
      soin(); console.value ='je mange';
    }
    if ( tamponConsole == infojouer ) {
      baseball(); console.value ='je joue';
    }
  }
});
window.addEventListener('keydown', (event) => {
  if (event.keyCode === 38) {
    h--;
    if (h <=0) {
      h=1;
    }
    console.value = tabCon[h];
    // document.getElementById("case"+h).style.color="red"
    // document.getElementById("case"+h).style.color="white"
  }
});
window.addEventListener('keydown', (event) => {
  if (event.keyCode === 40) {
    h++;
    if (h>=tabCon.length) {
      h=tabCon.length-1;
    }
    console.value = tabCon[h];
    // document.getElementById("case"+h).style.color="red"
    //  document.getElementById("case"+h).style.color="white"
  }
});


window.addEventListener('keyup', (event) => {
  if (event.keyCode === 13) {
    console.value = '';
  }
});

onglet.addEventListener('click', ouvertureOnglet);

function ouvertureOnglet() {
  if (consoleOnOff == false) {
    consoleOnOff = true;
    onglet.style.animationName = 'onglet';
    onglet.style.animationDuration = '2s';
    onglet.style.animationFillMode =' forwards';

    console.style.animationName = 'console';
    console.style.animationDuration = '2s';
    console.style.animationFillMode =' forwards';

    zonetexte.style.animationName = 'textconsole';
    zonetexte.style.animationDuration = '2s';
    zonetexte.style.animationFillMode =' forwards';
  } else {
    consoleOnOff = false;

    onglet.style.animationName = 'closeOnglet';
    onglet.style.animationDuration = '2s';
    onglet.style.animationFillMode =' forwards';

    console.style.animationName = 'closeConsole';
    console.style.animationDuration = '2s';
    console.style.animationFillMode =' forwards';

    zonetexte.style.animationName = 'texteconsoleclose';
    zonetexte.style.animationDuration = '2s';
    zonetexte.style.animationFillMode =' forwards';
  }
}
function pluiestart() {
  if ( pluieonoff === false) {
    eauico.src ='images/eau.png';
    varsoif = +3;
    pluieonoff = true;
    nuage1.style.animation = 'pluie 5s forwards';
    nuage2.style.animation='pluie 7s forwards';
    pluie.style.animation='goutte 7s forwards';
    if (niveausoif >= 200) {
      varsoif = -2;
    }
    parlersoif();
  } else {
    eauico.src ='images/starteau.png';
    varsoif = -2;
    pluieonoff = false;
    nuage1.style.animation='pluiefin 7s forwards';
    nuage2.style.animation='pluiefin 7s forwards';
    pluie.style.animation='gouttefin 7s forwards';
    parlersoif2();
  }
}
function parlersoif() {
  etat.style.animationName='etat';
  etat.style.animationDuration='5s';
  etat.style.animationIterationCount ='1';
  etat.innerText='Dieu merci! Il pleut ! Je commencais à avoir soif ...';
}
function parlersoif2() {
  etat.style.animationName='etat2';
  etat.style.animationDuration='5s';
  etat.style.animationIterationCount ='1';
  etat.innerText='Toute cette pluie .. Je suis tout ramolli !';
}

function soin() {
  paracrate.style.visibility='visible';
  enterName.style.visibility='hidden';
  paracrate.src='images/monparachute.png';
  paracrate.classList.remove('animate');
  void paracrate.offsetWidth; // trigger a DOM reflow
  paracrate.classList.add('animate');
  mafonction;
  function mafonction() {
    paracrate.src='images/monparachutefin.png';
    ver.classList.remove('versoin');
    void ver.offsetWidth; // trigger a DOM reflow
    ver.classList.add('versoin');

    mafonction2;
    function mafonction2() {
      enterName.style.visibility='visible';
      paracrate.style.visibility='hidden';
      if (niveausante < 150) {
        niveausante=niveausante+50;
      } else {
        niveausante = 200;
      }
    }

    setTimeout(mafonction2, 3000);
  }

  setTimeout(mafonction, 5000);
}

